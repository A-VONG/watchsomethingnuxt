import axios from 'axios';
import Axios from './callService';

//Recuperer les animes dans la bdd
let getAllAnime = () => {
    return Axios.get('/Anime').then((res) => {
        return res.data
    })
    .catch((err) => console.log(err));
};

//Get un api qui contient tout les apis
let listAnime = () => {
    return axios.get('https://animechan.vercel.app/api/available/anime').then((res) => {
        return res.data
    })
    .catch((err) => console.log(err));
};

// Mettre tout les animes dans la bdd
let postAnimeList = (name:any) => {
    return Axios.post('/Anime', name).then((res) => {
        return res
    })
    .catch((err) => console.log(err));
}

export const testService = {
    getAllAnime,
    listAnime,
    postAnimeList
}